import java.util.Random;


public class App {
    public static void main(String[] args) throws Exception {
        double number1 = 2.100212;
        int n1 = 2;
        double number2 = 2.100212;
        int n2 = 3;
        int number3 = 2100;
        int n3 = 2;
        
        String roundedNumber1 = lamTronSo(number1, n1);
        String roundedNumber2 = lamTronSo(number2, n2);
        String roundedNumber3 = lamTronSo(number3, n3);

        System.out.println(roundedNumber1);  // Output: 2.10
        System.out.println(roundedNumber2);  // Output: 2.100
        System.out.println(roundedNumber3);  // Output: 2100.00


        System.out.println("----------------------------------------------------");
        int soThuNhat = 1;
        int soThuHai = 10;
        int soThuBa = 10;
        int soThuTu = 20;

        int ketQua1 = taoSoNgauNhienTrongKhoang(soThuNhat, soThuHai);
        int ketQua2 = taoSoNgauNhienTrongKhoang(soThuBa, soThuTu);

        System.out.println(ketQua1);
        System.out.println(ketQua2);

        System.out.println("----------------------------------------------------");

        int a1 = 2;
        int b1 = 4;
        int a2 = 3;
        int b2 = 4;

        double c1 = tinhSoPytago(a1, b1);
        double c2 = tinhSoPytago(a2, b2);

        System.out.println(c1);
        System.out.println(c2);

        System.out.println("----------------------------------------------------");
        int a = 64;
        int b = 94;
      
        System.out.println(kiemTraSoChinhPhuong(a)); // true
        System.out.println(kiemTraSoChinhPhuong(b)); // false

        System.out.println("----------------------------------------------------");
        int so1 = 32;
        int so2 = 137;
        
        int ketQuaThuNhat = laySoLonNhatChiaHetCho5(so1);
        int ketQuaThuHai = laySoLonNhatChiaHetCho5(so2);
        
        System.out.println("Output 1: " + ketQuaThuNhat);
        System.out.println("Output 2: " + ketQuaThuHai);

        System.out.println("----------------------------------------------------");
        int so = 12;
        String chuoi1 = "abcd";
        String chuoi2 = "12";

        boolean ketQua3 = laSo(so);
        boolean ketQua4 = laSo(chuoi1);
        boolean ketQua5 = laSo(chuoi2);

        System.out.println(ketQua3);
        System.out.println(ketQua4);
        System.out.println(ketQua5);

        System.out.println("----------------------------------------------------");
        int so3 = 16;
        int so4 = 18;
        int so5 = 256;
      
        System.out.println(kiemTraLuyThuaCuaHai(so3)); // true
        System.out.println(kiemTraLuyThuaCuaHai(so4)); // false
        System.out.println(kiemTraLuyThuaCuaHai(so5)); // true

        System.out.println("----------------------------------------------------");
        double so8 = -15;
        int so6 = 1;
        double so7 = 1.2;

        System.out.println(kiemTraSoTuNhien(so8));
        System.out.println(kiemTraSoTuNhien(so6));
        System.out.println(kiemTraSoTuNhien(so7));

        System.out.println("----------------------------------------------------");
        double soThuNam = 1000;
        double soThuSau = 10000.23;

        String ketQuaThuBa = themDauPhanNgan(soThuNam);
        String ketQuaThuTu = themDauPhanNgan(soThuSau);

        System.out.println(ketQuaThuBa);
        System.out.println(ketQuaThuTu);

        System.out.println("----------------------------------------------------");
        int decimalNumber = 51;
        int binaryNumber = chuyenThapPhanSangNhiPhan(decimalNumber);
        System.out.println(binaryNumber);

        int decimalNumber2 = 4;
        int binaryNumber2 = chuyenThapPhanSangNhiPhan(decimalNumber2);
        System.out.println(binaryNumber2);
        
    }

    //phương thức làm tròn số 
    public static String lamTronSo(double number, int n){
            String format = "%." + n + "f";
            return String.format(format, number);
    }

    //lấy số random
    public static int taoSoNgauNhienTrongKhoang(int gioiHanDuoi, int gioiHanTren) {
        Random random = new Random();
        int soNgauNhien = random.nextInt(gioiHanTren - gioiHanDuoi + 1) + gioiHanDuoi;
        return soNgauNhien;
    }
    

    //tính pytago
    public static double tinhSoPytago(int a, int b) {
        double c = Math.sqrt(a * a + b * b);
        return c;
    }

    //kiểm tra số chính phương
    public static boolean kiemTraSoChinhPhuong(int so) {
        int canBacHai = (int) Math.sqrt(so); // Tính căn bậc hai của số
        
        // Kiểm tra xem căn bậc hai có bằng số gốc hay không
        if (canBacHai * canBacHai == so) {
            return true; // Là số chính phương
        } else {
            return false; // Không phải số chính phương
        }
    }

    //Lấy số lớn hơn gần nhất của số đã cho chia hết cho 5
    public static int laySoLonNhatChiaHetCho5(int so) {
        int soLonNhatChiaHetCho5 = ((so / 5) + 1) * 5;
        return soLonNhatChiaHetCho5;
    }

    //kiểm tra đầu vaò có phải là số hay không 
    public static boolean laSo(int number) {
        try {
            Integer.parseInt(String.valueOf(number));
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    public static boolean laSo(String text) {
        try {
            Integer.parseInt(text);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    //Kiểm tra số đã cho có phải lũy thừa của 2 hay không
    public static boolean kiemTraLuyThuaCuaHai(int so) {
        if (so <= 0) {
            return false; // Trả về false nếu số nhỏ hơn hoặc bằng 0
        }
        
        while (so > 1) {
            if (so % 2 != 0) {
                return false; // Trả về false nếu số không chia hết cho 2
            }
            so = so / 2;
        }
        
        return true; // Trả về true nếu số là lũy thừa của 2
    }


    //Kiểm tra số đã cho có phải số tự nhiên hay không
    public static boolean kiemTraSoTuNhien(double so) {
        return (so >= 0 && so == Math.floor(so));
    }

    //Thêm dấu , vào phần nghìn của mỗi số 
    public static String themDauPhanNgan(double so) {
        String chuoiSo = String.format("%.2f", so);
        String[] phanNguyenVaPhanThapPhan = chuoiSo.split("\\.");

        String phanNguyen = phanNguyenVaPhanThapPhan[0];
        StringBuilder ketQua = new StringBuilder();

        int count = 0;
        for (int i = phanNguyen.length() - 1; i >= 0; i--) {
            ketQua.insert(0, phanNguyen.charAt(i));
            count++;
            if (count % 3 == 0 && i != 0) {
                ketQua.insert(0, ",");
            }
        }

        ketQua.append(".").append(phanNguyenVaPhanThapPhan[1]);
        return ketQua.toString();
    }

    //Chuyển số từ hệ thập phân về hệ nhị phân
    public static int chuyenThapPhanSangNhiPhan(int decimalNumber) {
        int binaryNumber = 0;
        int power = 1;

        while (decimalNumber > 0) {
            int bit = decimalNumber % 2;
            binaryNumber += bit * power;
            decimalNumber /= 2;
            power *= 10;
        }

        return binaryNumber;
    }


}
